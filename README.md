# Node.js simple file upload

### Dependencies
- express
- ejs
- fs
- multer
- aws-sdk
- multer-s3


### Run
```
> git clone git@bitbucket.org:RomanWendlinger/node-upload.git
> cd node-upload
> npm install
> npm start
```
The application will be served on `:3000`

### Docker

```bash
docker build -t node-upload-aws .

docker run -e AWS_ACCESS_KEY_ID="XXXXXXXXX" -e AWS_SECRET_ACCESS_KEY="XXXXXXXXXXX" -e AWS_BUCKET="XXXXXXXX" -p 3000:3000 node-upload-aws
``` 
