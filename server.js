var express = require('express');
var aws = require('aws-sdk')
var multer  = require('multer');
var cors = require('cors');
var fs  = require('fs');
var multerS3 = require('multer-s3');
var bodyparser = require('body-parser');



var app = express();
var s3 = new aws.S3();
app.set('view engine', 'ejs');

app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyparser.json({limit: '50mb'}));
app.use(bodyparser.urlencoded({limit: '50mb', extended: true}));

app.get('/', (req, res) => {
    res.render('index');
});
var awsStorage = {
    storage: multerS3({
        dir: "/connect-app-uploads",
        s3: s3,
        bucket: process.env.AWS_BUCKET,
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.originalname});
        },
        key: function (req, file, cb) {
            console.log(file);
            cb(null, file.originalname)
        }
    })
};
var upload = multer(awsStorage).single('file');
app.post('/upload', function (req, res, next) {


    console.log("req.file", req.file);
    const uploadRes = upload(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.json({
                "message": "Something went wrong: " + err.message,
                "req": JSON.stringify(req.body)
            });
        }
        res.json({"message": "Upload completed."});
        console.log("all went well ");
    })
})

app.listen(3000);
